package com.deveducation.ws;

import com.google.gson.Gson;
import com.deveducation.dto.Message;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.websocketx.*;
import com.deveducation.service.MessageService;

public class WebSocketHandler extends ChannelInboundHandlerAdapter {

    private final Gson gson = new Gson();

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

        if (msg instanceof WebSocketFrame) {

            System.out.println("Client Channel : " + ctx.channel());
            if (msg instanceof BinaryWebSocketFrame) {
                System.out.println("BinaryWebSocketFrame Received : " + ((BinaryWebSocketFrame) msg).content());
            } else if (msg instanceof TextWebSocketFrame) {

                Message message = gson.fromJson(((TextWebSocketFrame) msg).text(), Message.class);
                String response = MessageService.commandHandle(message);
                ctx.channel().writeAndFlush(new TextWebSocketFrame(response));

                System.out.println(((TextWebSocketFrame) msg).text());
            } else if (msg instanceof PingWebSocketFrame) {
                System.out.println("PingWebSocketFrame Received : " + ((PingWebSocketFrame) msg).content());

            } else if (msg instanceof PongWebSocketFrame) {
                System.out.println("PongWebSocketFrame Received : " + ((PongWebSocketFrame) msg).content());
            } else if (msg instanceof CloseWebSocketFrame) {
                System.out.println("CloseWebSocketFrame Received : ");
                System.out.println("ReasonText :" + ((CloseWebSocketFrame) msg).reasonText());
                System.out.println("StatusCode : " + ((CloseWebSocketFrame) msg).statusCode());
            } else {
                System.out.println("Unsupported WebSocketFrame");
            }
        }
    }
}
