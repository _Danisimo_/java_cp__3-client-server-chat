package com.deveducation.utils;

import com.deveducation.db.Dao;

import java.sql.*;

public class RegUtil {

    public static boolean createPerson(String username, String login, String password) throws SQLException {
        try {
            PreparedStatement statement = Dao.getConnection().prepareStatement(
                    "INSERT INTO users" +
                            "(firstName, surName, login, password, roomsId, photoUrl, statusonline)" +
                            "VALUES ( ?, ?, ?, ?, ?,  ?,?) "
            );
            statement.setString(1, username);
            statement.setString(2, "1");
            statement.setString(3, login);
            statement.setString(4, password);
            statement.setString(5, "id");
            statement.setString(6, "account.getPhotoUrl()");
            statement.setBoolean(7, true);
            statement.execute();
            System.out.println("Create: " + login);

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean checkPerson(String login, String password) {
        try {
            PreparedStatement preparedStatement =
                    Dao.getConnection().prepareStatement(
                            "SELECT * FROM users WHERE login =" + "'" + login + "'" + " AND password =" + "'" + password + "'"
                    );
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet == null) {
                return false;
            }
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
