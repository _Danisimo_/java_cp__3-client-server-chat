package com.deveducation.dto;

public class TokenResponse {
    Tokens tokens;
    String userId;

    public Tokens getTokens() {
        return tokens;
    }

    public String getUserId() {
        return userId;
    }

    public TokenResponse(Tokens tokens, String userId) {
        this.tokens = tokens;
        this.userId = userId;
    }

    public static class Tokens {
        String accessToken;
        String refreshToken;

        public Tokens(String accessToken, String refreshToken) {
            this.accessToken = accessToken;
            this.refreshToken = refreshToken;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public String getRefreshToken() {
            return refreshToken;
        }
    }
}
