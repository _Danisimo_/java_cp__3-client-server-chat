package com.deveducation.dto;

public enum Command {
    SENDMESSAGE,
    LOGIN,
    LOGOUT,
    CREATECHAT,
    REMOVECHAT,
    REGISTERUSER,
    STARTGAME,
    STOPGAME,
    CURRENT_USER,
    USERS,
    ROOMS;
}
