package com.deveducation.jwtToken;

import com.deveducation.db.RepositoryService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class TokenGenerator {
    private static final Logger logger = LogManager.getLogger(TokenGenerator.class);
    private static final Map<String, Long> tokenTimeExpired = new HashMap<String, Long>();
    private static final String secretKey = "Супер секретный ключ";

    public static String generateToken(String login, String password) {
        if (RepositoryService.verifyUser(login, password)) {
            Integer loginHashCode = login.hashCode();
            Integer passwordHashCode = password.hashCode();

            Mac hmacSha256 = null;
            try {
                hmacSha256 = Mac.getInstance("HmacSHA256");
            } catch (NoSuchAlgorithmException nsae) {
                logger.error("No such algorithm");
            }

            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), "HmacSHA256");

            try {
                hmacSha256.init(secretKeySpec);
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }
            // Build and return signature
            String token = Base64.getEncoder().encodeToString(hmacSha256.doFinal(
                    new byte[]{loginHashCode.byteValue(), passwordHashCode.byteValue()}));

            tokenTimeExpired.put(token, Instant.now().plus(1, ChronoUnit.HOURS).toEpochMilli());
            return token;

        } else throw new IllegalArgumentException("There is no such user");
    }

//    public static String generateTokenSignUp(String username, String login, String password) throws SQLException {
//        if (RepositoryService.verifyUserSignup(username, login, password)) {
//            Integer loginHashCode = login.hashCode();
//            Integer passwordHashCode = password.hashCode();
//            Integer usernameHashCode = username.hashCode();
//
//            Mac hmacSha256 = null;
//            try {
//                hmacSha256 = Mac.getInstance("HmacSHA256");
//            } catch (NoSuchAlgorithmException nsae) {
//                logger.error("No such algorithm");
//            }
//
//            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
//
//            try {
//                hmacSha256.init(secretKeySpec);
//            } catch (InvalidKeyException e) {
//                e.printStackTrace();
//            }
//            // Build and return signature
//            String token = Base64.getEncoder().encodeToString(hmacSha256.doFinal(
//                    new byte[]{usernameHashCode.byteValue(), loginHashCode.byteValue(), passwordHashCode.byteValue()}));
//
//            tokenTimeExpired.put(token, Instant.now().plus(1, ChronoUnit.HOURS).toEpochMilli());
//            return token;
//
//        } else throw new IllegalArgumentException("There is no such user");
//    }

    public static boolean isTokenValid(String token) {
        return tokenTimeExpired.containsKey(token);
    }

    public static Long addTimeExpirationToken(long tokenTime, String token) {
        return tokenTimeExpired.put(token,
                Instant.ofEpochMilli(tokenTimeExpired.get(token))
                        .plus(tokenTime, ChronoUnit.HOURS).toEpochMilli());
    }

    public static Long getTimeExpiration(String token) {
        return tokenTimeExpired.get(token);
    }
}