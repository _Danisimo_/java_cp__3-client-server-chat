package com.deveducation.db;

import com.deveducation.utils.RegUtil;

import java.sql.SQLException;

public class RepositoryService {

    public static boolean verifyUser(String login, String password) {
        return RegUtil.checkPerson(login, password);
//        return login.equals("dasha") ? password.equals("petya"): login.equals("vasya");
    }

    public static boolean verifyUserSignup(String username, String login, String password) throws SQLException {
        return RegUtil.createPerson(username, login, password);
    }
}
