import React, { useEffect, useCallback, useState, createRef, useRef } from 'react';
import { Box, Button, Input, List, ListItem, ListItemText } from '@material-ui/core';

const Main = ({ children }) => {
  const stompClient = useRef(null);
  const name = 'MaxZima';

  const [messages, setMessages] = useState([]);

  const inputRef = createRef();

  const sendMessage = (event) => {
    if (event.keyCode === 13 || event.type === 'click') {
      const messageContent = String(inputRef.current.value).trim();

      if (messageContent && stompClient.current) {
        const chatMessage = {
          sender: name,
          content: messageContent,
          type: 'CHAT',
        };

        stompClient.current.send('/app/chat.sendMessage', {}, JSON.stringify(chatMessage));
        inputRef.current.value = '';
      }
      event.preventDefault();
    }
  };

  const onMessageReceived = useCallback((payload) => {
    const message = JSON.parse(payload.body);

    if (message.type === 'newUser') {
      message.content = `${message.sender}has joined the chat`;
      setMessages((state) => [...state, message]);
    } else if (message.type === 'Leave') {
      message.content = `${message.sender}has left the chat`;
      setMessages((state) => [...state, message]);
    } else {
      setMessages((state) => [...state, message]);
    }
  }, []);

  const connectionSuccess = useCallback(() => {
    stompClient.current.subscribe('/topic/javainuse', onMessageReceived);

    stompClient.current.send(
      '/app/chat.newUser',
      {},
      JSON.stringify({
        sender: name,
        type: 'newUser',
      }),
    );
  }, []);

  useEffect(() => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    // stompClient.current = Stomp.over(new SockJS('http://localhost:8080/websocketApp'));
    stompClient.current.connect({}, () => connectionSuccess());
  }, []);

  return (
    <Box>
      {children}
      <Box className="chat-main">
        <Box>
          <List>
            {messages.map((message, index) => {
              return (
                <ListItem key={`${index + 1}`}>
                  <ListItemText primary={message.content} />
                  <ListItemText secondary={message.sender} />
                </ListItem>
              );
            })}
          </List>
        </Box>
      </Box>
      <Box>
        <Input inputRef={inputRef} onKeyUp={sendMessage} />
        <Button onClick={sendMessage}>Send</Button>
      </Box>
    </Box>
  );
};

export { Main };
